package com.example.imagechatapplication.connection.connectionstrategies;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.interfaces.ConnectionStrategy;
import org.jdeferred.Deferred;
import org.json.simple.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NodeServerConnectionStrategy implements ConnectionStrategy {




    /**
     * It creates an asynctask in the background, which will contact the server and fetch/operate on the data in the db
     *
     * @param params (Object) - they consist of:
     *            params[0] - should be json we want to send to the server
     *            params[1] - should be the current context (like when we are on a MainActivity, we send <this>
     *                  this context will be used to render on-screen responses and use some functions from activity
     *            params[2] - the route for the server. For example:
     *              /auth/register  will delegate the request to /auth/register route.
     * @return  a special object ConnectionWrapper, which consists of:
     *      context (Context) (used just like explained above),
     *      response (JSONObject) - the response from the server, converted to JSONObject
     *          response may be null in case of some error,  when the client does not get response from server
     *              when it is null, we should take that into consideration in postrequest method
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)              //we need that for the current build, may change in the future
    public ConnectionWrapper doInBackground(Object ...params) {
        JSONObject json = (JSONObject) params[0];
        Context v = (Context) params[1];
        Deferred deferred = (Deferred) params[3];
        try {
            String route = "http://cactichat-server.herokuapp.com"+params[2];
            //String route = "http://192.168.2.100:5000"+params[2];
            //String route = "http://192.168.43.186:5000"+params[2];
            Log.e("Tag", "Connecting to "+route+" ...");
            URL url = new URL(route);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");  urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type","application/json");
            DataOutputStream outToServer = new DataOutputStream(urlConnection.getOutputStream());
            outToServer.writeBytes(json.toString()+'\n');
            outToServer.close();
            Log.e("tag","sent data!");

            BufferedReader inFromServer  = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
            JSONObject response = ConnectionManager.getInstance().getFromServer(inFromServer.readLine());
            return new ConnectionWrapper(v, response, deferred);


        }catch(Exception e){
            Log.e("Tag","Exception when connecing to server!");
            e.printStackTrace();
            final Activity activity = (Activity)params[1];
            final ProgressBar mProgress = (ProgressBar)activity.findViewById(R.id.progressBar);

            if(mProgress != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgress.setVisibility(View.INVISIBLE);
                        //Toast.makeText(activity.getApplicationContext(), "Error connecting to server. Check your connectionUtils!" , Toast.LENGTH_LONG).show();
                    }
                });
            }
            return new ConnectionWrapper((Context) params[1], null, deferred);
        }
    }
}