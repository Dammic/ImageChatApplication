package com.example.imagechatapplication.connection.connectionTasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.Toast;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.Utils;
import org.jdeferred.Deferred;

/**
 * Created by Dammic on 30/05/2016.
 */
public class AddImageTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;
        String status = Utils.tryToGetStatus(w);
        if(status == null)  {deferred.resolve(null);  return;}
        switch (status) {
            case "ok":
                Toast.makeText(w.context, "Successfully sent the image to the selected friends!", Toast.LENGTH_LONG).show();
                Button sendButton = (Button)((Activity)w.context).findViewById(R.id.sendButton);
                sendButton.setEnabled(false);
                deferred.resolve(null);
                break;
            default:
                Toast.makeText(w.context, "Error while completing the request, please try again!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
        }
    }
}
