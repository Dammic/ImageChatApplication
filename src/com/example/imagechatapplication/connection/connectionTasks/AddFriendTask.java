package com.example.imagechatapplication.connection.connectionTasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.Utils;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;

/**
 * Created by Dammic on 30/05/2016.
 */
public class AddFriendTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;
        String status = Utils.tryToGetStatus(w);
        if(status == null)  {deferred.resolve(null);  return;}
        switch (status) {
            case "ok":
                final Activity actTmp = (Activity)w.context;
                ConnectionManager.getInstance().getConnectionUtils().refresh(actTmp).done(new DoneCallback() {
                    public void onDone(Object result) {
                        //Toast.makeText(w.context, "Successfully added a new friend to the friendList!", Toast.LENGTH_LONG).show();
                        deferred.resolve(null);
                    }
                });
                break;
            case "already":
                Toast.makeText(w.context, "This user is already on your friends list!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
            case "himself":
                Toast.makeText(w.context, "You are already your own best friend!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
            default:
                Toast.makeText(w.context, "Error while completing the request, please try again!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
        }
    }
}
