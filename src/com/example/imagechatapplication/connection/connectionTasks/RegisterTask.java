package com.example.imagechatapplication.connection.connectionTasks;

import android.os.AsyncTask;
import android.widget.Toast;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.SharedPreferencesService;
import org.jdeferred.Deferred;

public class RegisterTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;
        String status;
        try {
            status = (String) w.response.get("status");
        }catch(RuntimeException e){
            Toast.makeText(w.context, "There was a problem with connection to the server!", Toast.LENGTH_LONG).show();
            deferred.resolve(null);
            return;
        }
        switch (status) {
            case "taken":
                Toast.makeText(w.context, "Those creditals are taken!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
            case "ok":
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                sharedPreferencesService.setToken(w.context,(String)w.response.get("token"));
                deferred.resolve(null);
                break;
            default:
                Toast.makeText(w.context, "Error while completing the request, please try again!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;
        }
    }
}
