package com.example.imagechatapplication.connection.connectionTasks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.*;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.Utils;
import com.example.imagechatapplication.ui.fragment.imageDetailViewPreview.ImageDetailViewFragment;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.json.simple.JSONObject;

/**
 * Created by Dammic on 30/05/2016.
 */
public class GetImageDetailTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;
        String status = Utils.tryToGetStatus(w);
        if(status == null)  {deferred.resolve(null);  return;}
        switch (status) {
            case "ok":
                w.response = (JSONObject)w.response.get("data");
                byte[] imgBytes = Base64.decode((String)w.response.get("imageString"), Base64.NO_WRAP);
                Bitmap picture = BitmapFactory.decodeByteArray(imgBytes , 0, imgBytes.length);
                picture = Utils.rotateBitmap(picture, 90);
                final Activity actTmp = (Activity)w.context;
                Bitmap finalPicture = picture;

                //@todo: do we need to refresh now?
                ConnectionManager.getInstance().getConnectionUtils().refresh(actTmp).done(new DoneCallback() {
                    public void onDone(Object result) {
                        new ImageDetailViewFragment().showFragment((Activity)w.context, finalPicture);
                        deferred.resolve(null);
                    }
                });
                break;
            default:
                Toast.makeText(w.context, "Error while completing the request, please try again!", Toast.LENGTH_LONG).show();
                deferred.resolve(null);
                break;

        }
    }
}
