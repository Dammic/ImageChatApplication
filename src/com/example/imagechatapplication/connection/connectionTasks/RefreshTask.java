package com.example.imagechatapplication.connection.connectionTasks;

import android.os.AsyncTask;
import android.widget.*;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.classes.ImageInfo;
import com.example.imagechatapplication.connection.*;
import com.example.imagechatapplication.services.publicServices.Utils;
import org.jdeferred.Deferred;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class RefreshTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;

        ArrayList<FriendInfo> friendList = new ArrayList<>();
        ArrayList<FriendInfo> pendingFriendList = new ArrayList<>();
        ArrayList<ImageInfo> imageList = new ArrayList<>();

        String status = Utils.tryToGetStatus(w);
        if(status == null)  {deferred.resolve(null);  return;}
        switch (status) {
            case "ok":
                w.response = (JSONObject)w.response.get("data");
                //getting friendList
                friendList = Utils.convertJSONArrayToArrayListFriendInfo((JSONArray)w.response.get("friendsList"));

                //getting pendingFriendsList
                pendingFriendList = Utils.convertJSONArrayToArrayListFriendInfo((JSONArray)w.response.get("friendRequestsList"));

                //gettingImagesInfo
                imageList = Utils.convertJSONArrayToArrayListImageInfo((JSONArray)w.response.get("imagesArray"));
                break;
            default:
                //in case of database problem, the tables should be empty
                Toast.makeText(w.context, "Error while completing the request, please try again!", Toast.LENGTH_LONG).show();
                break;
        }
        ConnectionManager.getInstance().getUpdateSubject().updateFriendsList(friendList);
        ConnectionManager.getInstance().getUpdateSubject().updateFriendRequestsList(pendingFriendList);
        ConnectionManager.getInstance().getUpdateSubject().updateImagesList(imageList);

        deferred.resolve(null);

        /*
        //initializing adapters
        CustomFriendListAdapter adapter = new CustomFriendListAdapter(w.context, friendListArray);
        CustomImagesListAdapter adapterImages = new CustomImagesListAdapter(w.context, imageListArray);
        CustomFriendSelectedListAdapter adapterSelected = new CustomFriendSelectedListAdapter(w.context, friendListArray);
        CustomPendingFriendListAdapter adapterPending = new CustomPendingFriendListAdapter(w.context, pendingFriendListArray);
        //creating references to views
        ListView listFriendsView = (ListView)((Activity) w.context).findViewById(R.id.friendList);
        ListView listFriendsSelectedView = (ListView)((Activity) w.context).findViewById(R.id.friendSelectList);
        ListView listPendingFriendsView = (ListView)((Activity) w.context).findViewById(R.id.listPendingFriends);
        ListView listImageView = (ListView)((Activity) w.context).findViewById(R.id.listImages);
        //setting adapters
        listFriendsView.setAdapter(adapter);
        listFriendsSelectedView.setAdapter(adapterSelected);
        listPendingFriendsView.setAdapter(adapterPending);
        listImageView.setAdapter(adapterImages);
        */
    }
}
