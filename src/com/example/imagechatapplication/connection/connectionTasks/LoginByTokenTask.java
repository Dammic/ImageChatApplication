package com.example.imagechatapplication.connection.connectionTasks;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.ui.activity.loginactivity.LoginActivity;
import com.example.imagechatapplication.ui.activity.mainactivity.MainActivity;
import org.jdeferred.Deferred;

public class LoginByTokenTask extends AsyncTask<Object,ConnectionWrapper,ConnectionWrapper> {

    @Override
    protected ConnectionWrapper doInBackground(Object... params){
        return ConnectionManager.getInstance().getConnectionStrategy().doInBackground(params[0], params[1], params[2], params[3]);
    }

    @Override
    protected void onPostExecute(ConnectionWrapper w){
        Deferred deferred = w.deferred;
        Intent n;

        String status;
        try {
            status = (String) w.response.get("status");
        }catch(RuntimeException e){
            Toast.makeText(w.context, "There was a problem with connection to the server!", Toast.LENGTH_LONG).show();
            status = "error";
        }
        switch (status) {
            case "ok":
                n = new Intent(w.context, MainActivity.class);
                w.context.startActivity(n);
                deferred.resolve(null);
                ((Activity)w.context).finish();
                break;
            default:
                n = new Intent(w.context, LoginActivity.class);
                w.context.startActivity(n);
                deferred.resolve(null);
                ((Activity)w.context).finish();
                break;
        }
    }
}
