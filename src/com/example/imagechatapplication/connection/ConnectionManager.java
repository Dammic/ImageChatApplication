package com.example.imagechatapplication.connection;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import com.example.imagechatapplication.connection.connectionTasks.*;
import com.example.imagechatapplication.interfaces.ConnectionStrategy;
import com.example.imagechatapplication.connection.connectionstrategies.NodeServerConnectionStrategy;
import com.example.imagechatapplication.services.publicServices.SharedPreferencesService;
import com.example.imagechatapplication.services.publicServices.UpdateSubject;
import com.example.imagechatapplication.services.publicServices.Utils;
import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.IOException;
import java.util.ArrayList;

public class ConnectionManager extends Application{
    private static ConnectionManager instance;
    private ConnectionUtils connectionUtils;
    private ConnectionStrategy connectionStrategy;
    private UpdateSubject updateSubject;

    public void onCreate(){
        super.onCreate();
        if(instance == null)  instance = this;
        connectionUtils = new ConnectionUtils();
        connectionStrategy = new NodeServerConnectionStrategy();
    }

    public static ConnectionManager getInstance() {
        return instance;
    }
    public ConnectionStrategy getConnectionStrategy() {return connectionStrategy;}
    public ConnectionUtils getConnectionUtils() {
        return connectionUtils;
    }
    public UpdateSubject getUpdateSubject()  {
        if(updateSubject == null)  updateSubject = new UpdateSubject();
        return updateSubject;
    }
    public void clearUpdateSubject()  {updateSubject = null;}

    public JSONObject getFromServer(String response) throws IOException {
        Log.e("read", "Response:"+response);
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(response);
        } catch (ParseException e) {e.printStackTrace();}
        return json;
    }


    public class ConnectionUtils {
        private ConnectionUtils() {

        }

        public Promise register(String login, String password, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                json.put("login", login);
                json.put("password", password);
                new RegisterTask().execute(json,activity, "/auth/register", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }

        public Promise login(String login, String password, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                json.put("login", login);
                json.put("password", password);
                new LoginTask().execute(json,activity, "/auth/login", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }

        public Promise loginByToken(Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                new LoginByTokenTask().execute(json,activity, "/auth/loginByToken", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }

        public Promise refresh(Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
            json.put("token", sharedPreferencesService.getToken(activity));
            try {
                new RefreshTask().execute(json,activity, "/user/refresh", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }

        public Promise addFriend(String friendID, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("friendID", friendID);
                new AddFriendTask().execute(json,activity, "/user/addFriend", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }


        public Promise sendFriendRequest(String friendName, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("friendName", friendName);
                new AddFriendRequestTask().execute(json,activity, "/user/addFriendRequest", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }


        public Promise deleteFriend(String friendID, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("friendID", friendID);
                json.put("isFriendRequest", false);
                new DeleteFriendTask().execute(json,activity, "/user/removeFriend", deferred);
            } catch(Exception e) {
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }


        public Promise deleteFriendRequest(String friendID, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("friendID", friendID);
                json.put("isFriendRequest", true);
                new DeleteFriendRequestTask().execute(json,activity, "/user/removeFriend", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }


        public Promise sendImage(ArrayList friendsSelected, String imgString, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("recipientsList", Utils.convertArrayListToJsonArray(friendsSelected));
                json.put("imageString", imgString);
                new AddImageTask().execute(json,activity, "/image/sendImage", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }


        public Promise getImageDetail(String imageID, Activity activity){
            Deferred deferred = new DeferredObject();
            JSONObject json = new JSONObject();
            try {
                SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
                json.put("token", sharedPreferencesService.getToken(activity));
                json.put("imageID", imageID);
                new GetImageDetailTask().execute(json,activity, "/image/getImageDetail", deferred);
            } catch(Exception e){
                Log.e("error", e.toString());
            }
            return deferred.promise();
        }

        public void disconnect(Activity activity){
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesService();
            sharedPreferencesService.setToken(activity,"");
        }

    }
}
