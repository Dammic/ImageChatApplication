package com.example.imagechatapplication.services.applicationServices;

import android.graphics.BitmapFactory;
import android.hardware.Camera;
import com.example.imagechatapplication.services.publicServices.Utils;
import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

public class JpegPictureCallback implements Camera.PictureCallback {
    private Deferred deferred;

    public Promise getPromise()  {return deferred.promise();}

    public JpegPictureCallback()  {deferred = new DeferredObject();}


    //this takes a while to run, regardless what is done inside. It is just not fired immediately after taking a photo
    //that is why we return deferred instead of actual response, so the function will be able to wait for results
    //@todo: can we make it fire like...faster?
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        deferred.resolve(Utils.rotateBitmap(BitmapFactory.decodeByteArray(data, 0, data.length), 90));
    }
}
