package com.example.imagechatapplication.services.applicationServices;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.example.imagechatapplication.ui.dialog.textSettingsDialog.TextSettingsDialogHandler;

import java.util.ArrayList;

/**
 * @param SCROLL_THRESHOLD (float) - the less, the more fluent the movement of the editText is,
 *        but also the less of it, the smaller movement may cause movement event instead of onclick
 * @param x_cord (int) - the previous x position. used to compare it with the current (and check if it's within a SCROLL_THRESHOLD)
 * @param y_cord (int) - see above, but y
 */
public class TextDragListener implements View.OnTouchListener {
    private int statusBarHeight;
    private int screenHeight;
    private int screenWidth;
    private FrameLayout.LayoutParams params;
    private DisplayMetrics displaymetrics;
    private final float SCROLL_THRESHOLD = 10;
    private int x_cord = 0;
    private int y_cord = 0;
    private Handler handler;               //the handler to execute the below function with a delay (kind of a manager)
    private Runnable longPressedHandler;   //the function that will execute after a long press (~1 sec)
    private boolean isOnClick[];           //dirty trick. The value you are looking for is [0]. The array is only for mutability - wrapper (we pass it
                                           //       to other class, which changes it, and primitive types are immutable
                                           //       on long click, this is set externally to false by the TextSettingsDialogHandler run() function

    public TextDragListener(View observedElement, ArrayList textEditsList){
        handler = new Handler();
        isOnClick = new boolean[1];
        longPressedHandler = new TextSettingsDialogHandler(observedElement, isOnClick, textEditsList);
        displaymetrics = new DisplayMetrics();
    }






    /**
     * In onTouchEvent on every ACTION_DOWN schedule a Runnable to run in 1 second.
     * On every ACTION_MOVE and ACTION_UP, cancel scheduled Runnable.
     * If cancelation happens less than 1s from ACTION_DOWN event, Runnable won't run.
     * That way we can see if the user pressed the element for x seconds
     * @todo: important! What will happen if we set position instead of margin? (will the textEdit be easier to position on a bitmap while sending a photo?)
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (MotionEvent.ACTION_MASK & event.getAction()) {  //the second parameter for some reason makes moving the element much smoother
            //that will happen when we initialize touch (put the finger on an object)
            case MotionEvent.ACTION_DOWN:
                params = (android.widget.FrameLayout.LayoutParams) v.getLayoutParams();
                isOnClick[0] = true;
                x_cord = (int)event.getX();
                y_cord = (int)event.getY();
                statusBarHeight = getStatusBarHeight(v);
                handler.postDelayed(longPressedHandler, 500);

                break;
            case MotionEvent.ACTION_MOVE:
                if (Math.abs(x_cord - event.getX()) > SCROLL_THRESHOLD || Math.abs(y_cord - event.getY()) > SCROLL_THRESHOLD) {
                    //actions we want to do only once!
                    if(isOnClick[0]){
                        Log.e("one","time action!");
                        handler.removeCallbacks(longPressedHandler);
                        ((EditText)v).setCursorVisible(false);
                        isOnClick[0] = false;
                    }
                    Log.e("action","action");

                    ((Activity)v.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                    /**
                     * todo:
                     * The height setting below works fantastic for my screen and all fonts, but as there is a magic number
                     * involved, this needs to be checked on another phone. Just check if you can place the text out of bottom
                     * part of the screen, please.
                     *
                     * The boundary is as follows: the size of the screen (1280 on my ph.),
                     * minus statusBar size*2 (don't know why *2, but it kind of works),
                     * AND minus the font size of the editText (which is the height of a single line)
                     *
                     */


                    screenHeight = displaymetrics.heightPixels - statusBarHeight*2 - (int)((EditText)v).getTextSize();
                    screenWidth = displaymetrics.widthPixels - (v.getWidth());
                    params.leftMargin = (int)event.getRawX() - (v.getWidth()/2);
                    params.topMargin = (int)event.getRawY() - (int)((EditText)v).getTextSize();
                    if(params.leftMargin < 0)  params.leftMargin = 0;
                     else if(params.leftMargin > screenWidth)  params.leftMargin = screenWidth;
                    if(params.topMargin < 0)  params.topMargin = 0;
                     else if(params.topMargin > screenHeight)  params.topMargin = screenHeight;
                    v.setLayoutParams(params);
                }
                break;
            case MotionEvent.ACTION_UP:
                handler.removeCallbacks(longPressedHandler);
                if (isOnClick[0]) {
                    ((EditText)v).setCursorVisible(true);
                    v.requestFocus();
                    ((InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            default:
                break;
        }
        return true;
    }


    private int getStatusBarHeight(View v) {
        int result = 0;
        int resourceId = (v.getContext()).getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)  result = (v.getContext()).getResources().getDimensionPixelSize(resourceId);
        return result;
    }

}
