package com.example.imagechatapplication.services.publicServices;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class GPSLocationHandler {
    Activity activity;
    AlertDialog alert;

    public GPSLocationHandler(Activity activity){
        this.activity = activity;
    }

    public void start(){
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER))  buildAlertMessageNoGps();

        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}
            @Override public void onProviderEnabled(String arg0) {}
            @Override public void onProviderDisabled(String arg0) {}
            @Override public void onLocationChanged(Location arg0) {
                Log.e("GPS NOTIFICATION", "GPS localization changed!");
            }
        });
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, final int id) {
                activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        alert = builder.create();
        alert.show();
    }

    public boolean isAlertVisible(){return alert != null;}
    public void dismissAlert()  {alert.dismiss();}


}
