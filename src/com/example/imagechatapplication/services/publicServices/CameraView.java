package com.example.imagechatapplication.services.publicServices;

import java.util.List;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import com.example.imagechatapplication.services.applicationServices.JpegPictureCallback;
import org.jdeferred.Promise;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback{
	
	private SurfaceHolder mHolder;
	private Camera mCamera;
    private ViewGroup layout;

    public Camera getCamera()  {return mCamera;}
    public void setCamera(Camera mCamera){this.mCamera = mCamera;}


    /**
     * @param context (Context or Activity) - the current context (mostly this or getActivity() or v.getContext())
     * @param layout (ViewGroup) - the layout we want to draw the camera on, eg. FrameLayout,
     */
    //@todo: http://stackoverflow.com/questions/3852154/android-camera-unexplainable-rotation-on-capture-for-some-devices-not-in-exif
    //@todo: basically the rotation is dependant on the device. So we want to make the rotation dependant on the position of the photo after taking it (look at exif and get that information)
	public CameraView(Context context, ViewGroup layout){
        super(context);
        mCamera = Camera.open();    //if we use Camera.open(int), we can choose the camera by id
        mCamera.setDisplayOrientation(90);
        setCameraDefaults();

        //get the holder and set this class as the callback, so we can get camera data here
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        attachCameraToSurface(layout);
    }


	private void setCameraDefaults(){
	    Camera.Parameters params = mCamera.getParameters();
	
	    // Supported picture formats (all devices should support JPEG).
	    List<Integer> formats = params.getSupportedPictureFormats();
	
	    if (formats.contains(ImageFormat.JPEG)) {
	        params.setPictureFormat(ImageFormat.JPEG);
	        params.setJpegQuality(100);
	    } else  params.setPictureFormat(PixelFormat.RGB_565);
	
	    // Now the supported picture sizes.
	    List<Size> sizes = params.getSupportedPictureSizes();
	    Camera.Size size = sizes.get(sizes.size()-1);
	    params.setPictureSize(size.width, size.height);
	
	    // Set the brightness to auto.
	    params.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
	
	    // Set the flash mode to auto.
	    params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
	
	    // Set the scene mode to auto.
	    params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
	
	    // Lastly set the focus to auto.
	    params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
	
	    mCamera.setParameters(params);
	}


    public Promise captureCameraBitmap(){
        JpegPictureCallback jpegCallback = new JpegPictureCallback();
        mCamera.takePicture(null, null, jpegCallback);
        return jpegCallback.getPromise();
    }
	
	@Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try{
            //when the surface is created, we can set the camera to draw images in this surfaceholder
            Log.e("Camera","Surface created!");
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e) {
            Log.d("ERROR", "Camera error on surfaceCreated " + e.getMessage());
        }
    }

    @Override public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {}

    public void attachCameraToSurface(ViewGroup layout){
        this.layout = layout;
        layout.removeAllViews();
        layout.addView(this);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        /*if(mCamera!=null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);
            
            mCamera.release();
            mCamera = null;
        }*/
    }


    public void releaseCamera(){
        Log.e("Camera","Released!");
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            setCamera(null);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener){
        layout.setOnClickListener(onClickListener);
    }
}