package com.example.imagechatapplication.services.publicServices;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.widget.Toast;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.classes.ImageInfo;
import com.example.imagechatapplication.classes.ConnectionWrapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class Utils {


  public static JSONObject getJSONObjectFromArrayListElement(Object elem) {
		JSONObject obj = new JSONObject();
        obj.put("userName", (String)elem);
		return obj;
  }

  public static JSONArray convertArrayListToJsonArray(ArrayList list){
	  JSONArray jsonArray = new JSONArray();
      for(Object aList : list){
          jsonArray.add(aList);
      }
      return jsonArray;
  }



    public static ArrayList<String> convertJSONArrayToArrayList(JSONArray array) {
        ArrayList<String> list = new ArrayList<String>();
        if (array != null) {
            for (int i = 0; i < array.size(); i++)  list.add(array.get(i).toString());
        }
        return list;
    }

    public static ArrayList<ImageInfo> convertJSONArrayToArrayListImageInfo(JSONArray array) {
        ArrayList<ImageInfo> list = new ArrayList<ImageInfo>();
        if (array != null) {
            for (int i = 0; i < array.size(); i++) {
                list.add(new ImageInfo(((JSONObject)(array.get(i))).get("_id").toString(), ((JSONObject)(array.get(i))).get("senderName").toString(), ((JSONObject)(array.get(i))).get("senderID").toString(), ((JSONObject)(array.get(i))).get("sentDate").toString()));
            }
        }
        return list;
    }

    public static ArrayList<FriendInfo> convertJSONArrayToArrayListFriendInfo(JSONArray array) {
        ArrayList<FriendInfo> list = new ArrayList<FriendInfo>();
        if (array != null) {
            for (int i = 0; i < array.size(); i++) {
                list.add(new FriendInfo(((JSONObject)(array.get(i))).get("_id").toString(), ((JSONObject)(array.get(i))).get("userName").toString()));
            }
        }
        return list;
    }



    public static Bitmap rotateBitmap(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static String getImageString(Bitmap image){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        (Utils.rotateBitmap(image, -90)).compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        try {stream.flush();}   catch (IOException e1)   {e1.printStackTrace();}
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }



	public static String tryToGetStatus(ConnectionWrapper w){
		try {
			return (String) w.response.get("status");
		}catch(Exception e){
			Toast.makeText(w.context, "There was a problem with connection to the server!", Toast.LENGTH_LONG).show();
			return null;
		}

	}


    // Accept only non-emoticon letters, letter & digits
    public static InputFilter getEmotsAndWhitespaceFilter(){
        return new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    int type = Character.getType(source.charAt(i));
                    if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL || Character.isWhitespace(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };


    }
}
