package com.example.imagechatapplication.services.publicServices;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.ui.dialog.loaderDialog.LoaderDialog;

/**
 * Created by dammic on 29.06.16.
 */
public class Loader {
    private Activity activity;
    private LoaderDialog loaderDialog;


    public Loader(Activity activity){
        this.activity = activity;
        loaderDialog = new LoaderDialog(activity);
        //loaderDialog.setTitle("Processing...");
        //loaderDialog.setMessage("Please wait...");
        loaderDialog.setCanceledOnTouchOutside(false);
        loaderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loaderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loaderDialog.setContentView(R.layout.dialog_loader);
    }

    public void show(){
        loaderDialog.show();
    }

    public void hide(){
        loaderDialog.dismiss();

    }




}
