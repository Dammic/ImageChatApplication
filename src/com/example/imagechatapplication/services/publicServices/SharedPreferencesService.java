package com.example.imagechatapplication.services.publicServices;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SharedPreferencesService {


    public void setToken(Context v, String token){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(v);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("token",token);
        editor.apply();
    }

    public String getToken(Context v){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(v);
        return settings.getString("token", ""/*default value*/);
    }
}
