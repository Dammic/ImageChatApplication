package com.example.imagechatapplication.services.publicServices;

import android.util.Log;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.classes.ImageInfo;
import com.example.imagechatapplication.interfaces.UpdateObserver;

import java.util.ArrayList;


public class UpdateSubject {
    private ArrayList<UpdateObserver> friendsListObservers;
    private ArrayList<UpdateObserver> friendRequestsListObservers;
    private ArrayList<UpdateObserver> imagesListObservers;

    private ArrayList<FriendInfo> friendsList;
    private ArrayList<ImageInfo> imagesList;
    private ArrayList<FriendInfo> friendRequestsList;


    public UpdateSubject(){
        friendsList = new ArrayList<>();
        imagesList = new ArrayList<>();
        friendRequestsList = new ArrayList<>();

        friendsListObservers = new ArrayList<>();
        friendRequestsListObservers = new ArrayList<>();
        imagesListObservers = new ArrayList<>();
    }



    ////////////************************\\\\\\\\\\\\\
    ////////////*Manipulating observers*\\\\\\\\\\\\\
    ////////////************************\\\\\\\\\\\\\

    public void addFriendsListObserver(UpdateObserver observer){
        if(!friendsListObservers.contains(observer))  friendsListObservers.add(observer);
    }
    public void addFriendRequestsListObserver(UpdateObserver observer){
        if(!friendRequestsListObservers.contains(observer))  friendRequestsListObservers.add(observer);
    }
    public void addImagesListObserver(UpdateObserver observer){
        if(!imagesListObservers.contains(observer))  imagesListObservers.add(observer);
    }

    public void removeFriendsListObserver(UpdateObserver observer)  {friendsListObservers.remove(observer);}
    public void removeFriendRequestsListObserver(UpdateObserver observer)  {friendRequestsListObservers.remove(observer);}
    public void removeImagesListObserver(UpdateObserver observer)  {imagesListObservers.remove(observer);}


    public void notify(ArrayList observerList, String updatedObject){
        Log.e("notify","Change detected in "+updatedObject+"! Notyfying " + observerList.size()+" observers!");
        for(Object observer : observerList)   ((UpdateObserver)observer).update(updatedObject);
    }


    ////////////************************\\\\\\\\\\\\\
    ////////////***Manipulating lists***\\\\\\\\\\\\\
    ////////////************************\\\\\\\\\\\\\


    public ArrayList<FriendInfo> getCurrentFriendsList()  {return friendsList;}
    public ArrayList<FriendInfo> getCurrentFriendRequestsList()  {return friendRequestsList;}
    public ArrayList<ImageInfo> getCurrentImagesList()  {return imagesList;}


    public void updateFriendsList(ArrayList list){
        friendsList = list;
        notify(friendsListObservers, "friendsList");
    }
    public void updateFriendRequestsList(ArrayList list){
        friendRequestsList = list;
        notify(friendRequestsListObservers, "friendRequestsList");
    }
    public void updateImagesList(ArrayList list){
        imagesList = list;
        notify(imagesListObservers, "imagesList");
    }
}
