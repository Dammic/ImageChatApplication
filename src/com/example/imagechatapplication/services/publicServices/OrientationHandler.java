package com.example.imagechatapplication.services.publicServices;

import android.app.Activity;
import android.hardware.SensorManager;
import android.view.OrientationEventListener;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;

//@todo: the orientation changes affect only one direction. Change it to handle two rotation directions
public class OrientationHandler {
    private Activity activity;

    public static final int ORIENTATION_PORTRAIT_NORMAL =  1;
    public static final int ORIENTATION_LANDSCAPE_NORMAL =  3;
    private int mOrientation =  -1;
    private OrientationEventListener mOrientationEventListener;
    private ResponsiveFragment fragment;


    public OrientationHandler(Activity activity, ResponsiveFragment fragment){
        this.activity = activity;
        this.fragment = fragment;
    }

    public void stop()  {mOrientationEventListener.disable();}
    public void start(){
        if (mOrientationEventListener == null) {
            mOrientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_NORMAL) {
                @Override
                public void onOrientationChanged(int orientation) {

                    // determine our orientation based on sensor response
                    int lastOrientation = mOrientation;

                    //because we do not want to trigger the change if there was no real change, we need to check the degrees
                    if (orientation >= 315 || orientation < 45) {
                        if (mOrientation != ORIENTATION_PORTRAIT_NORMAL)  mOrientation = ORIENTATION_PORTRAIT_NORMAL;
                    }else if (orientation < 315 && orientation >= 225) {
                        if (mOrientation != ORIENTATION_LANDSCAPE_NORMAL) mOrientation = ORIENTATION_LANDSCAPE_NORMAL;
                    }

                    if (lastOrientation != mOrientation) {
                        changeRotation(mOrientation);
                    }
                }
            };
        }
        if (mOrientationEventListener.canDetectOrientation()) {
            mOrientationEventListener.enable();
        }
    }


    @SuppressWarnings("deprecation")
    private void changeRotation(int orientation) {
        fragment.changeOrientation(orientation);

    }

}
