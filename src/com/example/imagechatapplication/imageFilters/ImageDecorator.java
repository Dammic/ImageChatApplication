package com.example.imagechatapplication.imageFilters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.view.View;
import com.example.imagechatapplication.interfaces.Image;

public class ImageDecorator implements Image {
    private Image imageToBeDecorated;

    ImageDecorator(Image imageToBeDecorated){
        this.imageToBeDecorated = imageToBeDecorated;
    }

    @Override
    public Bitmap setPicture(Bitmap picture) {
        return imageToBeDecorated.setPicture(picture);
    }


    public Bitmap combineImages(Bitmap background, Bitmap foreground, View v, boolean isSecond) {

        int width = 0, height = 0;
        Bitmap cs;

        width = ((Activity) (v.getContext())).getWindowManager().getDefaultDisplay().getWidth();
        height = ((Activity) (v.getContext())).getWindowManager().getDefaultDisplay().getHeight();
        Log.e("dim: ","Width: "+width+ " Height: "+height);
        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(cs);
        background = Bitmap.createScaledBitmap(background, width, height, true);
        int[] coords = new int[2];
        v.getLocationOnScreen(coords);
        Log.e("Coords","Coords: "+coords[0]+ "|"+coords[1]);
        comboImage.drawBitmap(background, 0, 0, null);
        int minus = 85;
        if(isSecond)  minus = 10;
        comboImage.drawBitmap(foreground, coords[0], coords[1]-minus, null);

        return cs;
    }


}
