package com.example.imagechatapplication.imageFilters;

import android.graphics.Bitmap;
import android.widget.TextView;
import com.example.imagechatapplication.interfaces.Image;

/**
 * Created by Dammic on 2016-06-05.
 */
public class AddTextDecorator extends ImageDecorator{
    TextView textEdit;

    public AddTextDecorator (Image imageToBeDecorated, TextView  textEdit) {
        super(imageToBeDecorated);
        this.textEdit = textEdit;
    }

    @Override
    public Bitmap setPicture(Bitmap picture){
        textEdit.setDrawingCacheEnabled(true);
        return combineImages(super.setPicture(picture),Bitmap.createBitmap(textEdit.getDrawingCache()), textEdit, false);
    }

}
