package com.example.imagechatapplication.classes;

import android.content.Context;
import org.jdeferred.Deferred;
import org.json.simple.JSONObject;

public class ConnectionWrapper {
   public Context context;
   public JSONObject response;
   public Deferred deferred;

   public ConnectionWrapper(Context context, JSONObject response, Deferred deferred){
       this.context = context;
       this.response = response;
       this.deferred = deferred;
   }

}
