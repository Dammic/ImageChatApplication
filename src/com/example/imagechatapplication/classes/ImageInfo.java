package com.example.imagechatapplication.classes;

public class ImageInfo{
    public String senderID;
	public String sender;
	public String imageID;
	public String sentDate;
	
	public ImageInfo(String imageID, String sender, String senderID, String sentDate){
		this.senderID = senderID;
		this.sender = sender;
		this.imageID = imageID;
		this.sentDate = sentDate;
	}
	
}
