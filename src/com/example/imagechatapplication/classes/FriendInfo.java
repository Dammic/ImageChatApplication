package com.example.imagechatapplication.classes;

/**
 * Created by dammic on 24.06.16.
 */
public class FriendInfo {
    public String id;
    public String userName;

    public FriendInfo(String id, String userName){
        this.id = id;
        this.userName = userName;
    }

}
