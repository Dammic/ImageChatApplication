package com.example.imagechatapplication.ui.activity.mainactivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.GPSLocationHandler;
import com.example.imagechatapplication.services.publicServices.Loader;
import com.example.imagechatapplication.ui.fragment.viewGroup.ViewGroupFragment;

public class MainActivity extends FragmentActivity {

    private Loader loader;
    private GPSLocationHandler gpsLocationHandler;

	//================================================================\\
	//						System functions						  \\
	//						  OnSomething							  \\
	//================================================================\\
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

		ConnectionManager.getInstance().getUpdateSubject(); //this will create one if it is not present
        loader = new Loader(this);
		new ViewGroupFragment().showFragment(this);

		ConnectionManager.getInstance().getConnectionUtils().refresh(this);

        gpsLocationHandler = new GPSLocationHandler(this);
        gpsLocationHandler.start();
	}


	@Override
	protected void onDestroy(){
		super.onDestroy();
		if(gpsLocationHandler.isAlertVisible()) { gpsLocationHandler.dismissAlert(); }
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}


    //@todo: bug: the buttons don't get recovered after resume (do a photo and then minimize app)
	@Override protected void onResume() {
		super.onResume();
	}

	//=========================================================//
	//=========================================================//
	//=========================================================//
}
