package com.example.imagechatapplication.ui.activity.welcomeactivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;

public class WelcomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_welcome);
        ConnectionManager.getInstance().getConnectionUtils().loginByToken(this);
    }
}