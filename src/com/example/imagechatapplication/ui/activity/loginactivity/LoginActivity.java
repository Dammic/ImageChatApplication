package com.example.imagechatapplication.ui.activity.loginactivity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.Loader;
import com.example.imagechatapplication.ui.dialog.registerDialog.RegisterDialog;
import org.jdeferred.DoneCallback;
import static com.example.imagechatapplication.services.publicServices.Utils.getEmotsAndWhitespaceFilter;


public class LoginActivity extends Activity {
    private EditText login_loginBox;
    private EditText login_passwordBox;
    private Dialog registerDialog;
    private Loader loader;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //initializing special variables
        loader = new Loader(this);
        registerDialog = new RegisterDialog(this);
        registerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  //this makes the dialog not to have a title

        //initializing content views
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

		//initializing layout components
		login_loginBox =  (EditText)findViewById(R.id.login_loginBox);
		login_passwordBox = (EditText)findViewById(R.id.login_passwordBox);

        //setting filters to the inputs
        InputFilter[] emoticonAndWhitespaceFilter = new InputFilter[]{getEmotsAndWhitespaceFilter(), new InputFilter.LengthFilter(25)};
        login_loginBox.setFilters(emoticonAndWhitespaceFilter);
        login_passwordBox.setFilters(emoticonAndWhitespaceFilter);
	}


    //@todo: after clicking on register, the text should have a small frame, as well as change color to more dark (like in links)
    public void showRegisterModal(View v){
        registerDialog.show();
    }


	public void loginClicked(View v) {
		if(login_loginBox.length() < 5 || login_passwordBox.length() < 5 || login_passwordBox.length() > 20){
			Toast.makeText(v.getContext(), "Invalid login or password! (>=5,<=20)", Toast.LENGTH_LONG).show();
		}else{
            loader.show();
            ConnectionManager.getInstance().getConnectionUtils().login(login_loginBox.getText().toString(), login_passwordBox.getText().toString(), (Activity)v.getContext())
                .done(new DoneCallback() {
                    @Override public void onDone(Object o)  {loader.hide();}
                });
        }
	}
}