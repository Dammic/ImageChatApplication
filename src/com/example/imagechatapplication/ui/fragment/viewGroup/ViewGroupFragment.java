package com.example.imagechatapplication.ui.fragment.viewGroup;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.ui.activity.loginactivity.LoginActivity;
import com.example.imagechatapplication.ui.adapters.ViewGroupPagerAdapter;
import com.example.imagechatapplication.ui.fragment.cameraView.CameraViewFragment;
import com.example.imagechatapplication.ui.fragment.friendsViewFragment.FriendsViewFragment;
import com.example.imagechatapplication.ui.fragment.imagesViewFragment.ImagesViewFragment;
import static com.example.imagechatapplication.services.publicServices.OrientationHandler.ORIENTATION_LANDSCAPE_NORMAL;
import static com.example.imagechatapplication.services.publicServices.OrientationHandler.ORIENTATION_PORTRAIT_NORMAL;


public class ViewGroupFragment extends Fragment implements ResponsiveFragment {
    private View fragmentView;
    private OrientationHandler orientationHandler;
    private ViewGroupPagerAdapter pagerAdapter;

    private ViewPager viewGroupFrame;


    private ImageButton disconnectButton;
    private ImageButton refreshButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_main, container, false);

        return fragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //initializing layout components
        //viewGroupFrame = (FrameLayout)fragmentView.findViewById(R.id.viewGroupFrame);
        viewGroupFrame = (ViewPager)fragmentView.findViewById(R.id.viewGroupFrame);
        orientationHandler = new OrientationHandler(getActivity(), this);

        disconnectButton = (ImageButton)fragmentView.findViewById(R.id.disconnectButton);
        refreshButton = (ImageButton)fragmentView.findViewById(R.id.refreshButton);


        //setting listeners
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {disconnect(v);}
        });
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {refreshClicked(v);}
        });


        //redirecting to default fragment on viewGroupFrame - cameraView
        //new CameraViewFragment().showFragment(getActivity());

        //other settings
        orientationHandler.start();
        pagerAdapter = new ViewGroupPagerAdapter(((FragmentActivity)getActivity()).getSupportFragmentManager(), getActivity());
        viewGroupFrame.setAdapter(pagerAdapter);
        viewGroupFrame.setPageMargin(-1);
        viewGroupFrame.setCurrentItem(1);
    }


    @Override
    public void onDestroy(){
        Log.e("ViewGroup", "Destroy!");
        pagerAdapter.removeAllfragments();
        orientationHandler.stop();
        super.onDestroy();
    }


    public void disconnect(View v){
        ConnectionManager.getInstance().getConnectionUtils().disconnect(getActivity());
        Intent n = new Intent(getActivity(), LoginActivity.class);

        pagerAdapter.removeAllfragments();
        this.startActivity(n);
        getActivity().finish();
    }

    public void refreshClicked(View v){
        ConnectionManager.getInstance().getConnectionUtils().refresh(getActivity());
        Toast.makeText(getActivity(), "Refreshing...", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void changeOrientation(int orientation) {
        switch (orientation) {
            case ORIENTATION_PORTRAIT_NORMAL:
                break;
            case ORIENTATION_LANDSCAPE_NORMAL:
                break;
        }
    }


    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        if(params.length == 1) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.applicationFrame, this);
            ft.commit();
        }
    }

}
