package com.example.imagechatapplication.ui.fragment.imageDetailViewPreview;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.ui.fragment.viewGroup.ViewGroupFragment;

public class ImageDetailViewFragment extends Fragment implements ResponsiveFragment {
    private View fragmentView;
    private OrientationHandler orientationHandler;

    private Bitmap pictureBitmap;

    private ImageView imageBitmapView;
    private TextView timeLeftCount;
    private ProgressBar progressBar;
    private RelativeLayout detailView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_detailview, container, false);

        return fragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //initializing layout components
        detailView = (RelativeLayout)fragmentView.findViewById(R.id.detailView);
        imageBitmapView = (ImageView)fragmentView.findViewById(R.id.imageBitmapView);
        timeLeftCount = (TextView)fragmentView.findViewById(R.id.timeLeftCount);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.progressBar);

        //other settings
        orientationHandler = new OrientationHandler(getActivity(), this);
        orientationHandler.start();


        imageBitmapView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageBitmapView.setImageBitmap(pictureBitmap);

        CountDownTimer timer = new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
                int time = (int) Math.ceil((int) ((5 - (millisUntilFinished / 1000)) * 20));
                progressBar.setProgress(time);
                timeLeftCount.setText((5 - time / 20) + "");
            }

            public void onFinish()  {new ViewGroupFragment().showFragment(getActivity());}
        };
        timer.start();

        //setting listeners
        detailView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                timer.cancel();
                new ViewGroupFragment().showFragment(getActivity());
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        orientationHandler.stop();
    }

    @Override
    public void changeOrientation(int orientation) {

    }

    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        pictureBitmap = (Bitmap)params[1];
        if(params.length == 2) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.applicationFrame, this);
            ft.commit();
        }
    }


}
