package com.example.imagechatapplication.ui.fragment.cameraView;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.services.publicServices.CameraView;
import com.example.imagechatapplication.services.publicServices.Loader;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.ui.fragment.cameraPreview.CameraPreviewFragment;
import org.jdeferred.DoneCallback;

public class CameraViewFragment extends Fragment implements ResponsiveFragment {
    private View fragmentView;
    private Loader loader;
    private CameraView mCameraView = null;
    private OrientationHandler orientationHandler;

    private FrameLayout cameraAnchor;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_viewgroup_camera, container, false);

        return fragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

            //initializing layout components
            cameraAnchor = (FrameLayout)fragmentView.findViewById(R.id.cameraAnchor);
            loader = new Loader(getActivity());
            orientationHandler = new OrientationHandler(getActivity(), this);

            //other settings
            try  {
                mCameraView = new CameraView(fragmentView.getContext(), cameraAnchor);
                Log.e("mmcamera", mCameraView+"");
            } catch (Exception e){Log.e("ERROR", "Failed to get camera: " + e.getMessage());}
            //setting listeners
            setCameraListener();

            //cameraAnchor.removeViewAt(0);
            //cameraAnchor = (FrameLayout)fragmentView.findViewById(R.id.cameraAnchor);
            //mCameraView.attachCameraToSurface(cameraAnchor);


        Log.e("CameraView","onViewCreated x time");
        orientationHandler.start();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e("CameraView", "onDestroy!");
        mCameraView.releaseCamera();
        orientationHandler.stop();
    }


    //@todo: bug: the buttons don't get recovered after resume (do a photo and then minimize app)
    @Override
    public void onResume() {
        super.onResume();
        Log.e("cameraView", "OnResume!");
        mCameraView.getCamera().startPreview();
    }


    @Override
    public void onPause() {
        super.onResume();
        Log.e("cameraView", "OnPause!");
        mCameraView.getCamera().stopPreview();
    }


    public void setCameraListener(){
        mCameraView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {takePicture(v);}
        });
    }


    //@todo: bug: clicking multiple times on camera after taking photo yields to crash (will be fixed with the introduction to fragments
    public void takePicture(View v){
        mCameraView.captureCameraBitmap().done(new DoneCallback() {
            @Override
            public void onDone(Object o) {
                new CameraPreviewFragment().showFragment(getActivity(), o);
            }
        });
    }

    @Override
    public void changeOrientation(int orientation) {

    }


    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        if(params.length == 1) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.viewGroupFrame, this);
            ft.commit();
        }
    }

}
