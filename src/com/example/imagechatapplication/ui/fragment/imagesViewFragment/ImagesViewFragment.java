package com.example.imagechatapplication.ui.fragment.imagesViewFragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.ImageInfo;
import com.example.imagechatapplication.ui.adapters.CustomImagesListAdapter;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.interfaces.UpdateObserver;

import java.util.ArrayList;
import java.util.Arrays;

public class ImagesViewFragment extends Fragment implements ResponsiveFragment, UpdateObserver {
    private View fragmentView;

    private ListView listImages;
    private OrientationHandler orientationHandler;
    CustomImagesListAdapter adapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_viewgroup_images, container, false);

        return fragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //initializing layout components
        listImages = (ListView)fragmentView.findViewById(R.id.listImages);

        register();

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listImages.addHeaderView(inflater.inflate(R.layout.listheader_images_list, null));

        update("imagesList");

        orientationHandler = new OrientationHandler(getActivity(), this);
        orientationHandler.start();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        orientationHandler.stop();
        unregister();
    }


    @Override
    public void changeOrientation(int orientation) {

    }


    @Override
    public void update(String observedObject) {
        if(observedObject.compareTo("imagesList") == 0) {
            Object[] dataObjectArray = ((ArrayList) ConnectionManager.getInstance().getUpdateSubject().getCurrentImagesList()).toArray();
            ImageInfo[] data = Arrays.copyOf(dataObjectArray, dataObjectArray.length, ImageInfo[].class);
            adapter = new CustomImagesListAdapter(getActivity(), data);
            listImages.setAdapter(adapter);
        }
    }

    @Override public void register() {ConnectionManager.getInstance().getUpdateSubject().addImagesListObserver(this);}
    @Override public void unregister()  {ConnectionManager.getInstance().getUpdateSubject().removeImagesListObserver(this);}


    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        if(params.length == 1) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.viewGroupFrame, this);
            ft.commit();
        }
    }


}
