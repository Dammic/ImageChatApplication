package com.example.imagechatapplication.ui.fragment.selectFriendsViewFragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.services.publicServices.Utils;
import com.example.imagechatapplication.imageFilters.AddTextDecorator;
import com.example.imagechatapplication.interfaces.Image;
import com.example.imagechatapplication.imageFilters.SimpleImage;
import com.example.imagechatapplication.ui.adapters.CustomFriendSelectedListAdapter;
import com.example.imagechatapplication.services.publicServices.Loader;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.interfaces.UpdateObserver;
import com.example.imagechatapplication.ui.fragment.viewGroup.ViewGroupFragment;
import org.jdeferred.DoneCallback;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectFriendsViewFragment extends Fragment implements ResponsiveFragment, UpdateObserver {
    private View fragmentView;
    Loader loader;
    private OrientationHandler orientationHandler;
    CustomFriendSelectedListAdapter adapter;
    private String imageString;

    private ListView recipientsList;
    private Button sendButton;
    private Button backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_select_recipients, container, false);

        return fragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //initializing layout components
        recipientsList = (ListView)fragmentView.findViewById(R.id.recipientsList);
        sendButton = (Button)fragmentView.findViewById(R.id.sendButton);
        backButton = (Button)fragmentView.findViewById(R.id.backButton);

        //setting listeners
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {sendImage(v);}
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {backClicked(v);}
        });


        //other settings
        loader = new Loader(getActivity());
        orientationHandler = new OrientationHandler(getActivity(), this);
        orientationHandler.start();

        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        recipientsList.addHeaderView(inflater.inflate(R.layout.listheader_selects_list, null));
        update("friendsList");

        register();

    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        orientationHandler.stop();
        unregister();
    }


    @Override
    public void update(String observedObject) {
        if(observedObject.compareTo("friendsList") == 0) {
            Object[] dataObjectArray = ((ArrayList) ConnectionManager.getInstance().getUpdateSubject().getCurrentFriendsList()).toArray();
            FriendInfo[] data = Arrays.copyOf(dataObjectArray, dataObjectArray.length, FriendInfo[].class);
            adapter = new CustomFriendSelectedListAdapter(getActivity(), data);
            recipientsList.setAdapter(adapter);
        }
    }

    @Override public void register() {ConnectionManager.getInstance().getUpdateSubject().addFriendsListObserver(this);}
    @Override public void unregister()  {ConnectionManager.getInstance().getUpdateSubject().removeFriendsListObserver(this);}




    public void backClicked(View v){
        clearAllSelections();
        new ViewGroupFragment().showFragment(getActivity());
    }


    public String generateImageString(Bitmap pictureBitmap, TextView coordsText, ArrayList textViewsList){
        Image image = new AddTextDecorator(new SimpleImage(), coordsText);
        for (Object aTextList : textViewsList)  image = new AddTextDecorator(image, (TextView) aTextList);
        Image finalImage = image;
        return Utils.getImageString(finalImage.setPicture(pictureBitmap));
    }

    public void sendImage(View v){
        loader.show();
        ArrayList friendsSelected = ((CustomFriendSelectedListAdapter)((HeaderViewListAdapter)recipientsList.getAdapter()).getWrappedAdapter()).friendsSelected;

        ConnectionManager.getInstance().getConnectionUtils().sendImage(friendsSelected, imageString, getActivity())
        .done(new DoneCallback() {
            @Override public void onDone(Object o)  {
                ((CustomFriendSelectedListAdapter)((HeaderViewListAdapter)recipientsList.getAdapter()).getWrappedAdapter()).friendsSelected = new ArrayList<String>();

                backClicked(v);
                loader.hide();
            }
        });
    }


    public void clearAllSelections(){
        CheckBox et;
        for (int i=0; i<recipientsList.getCount(); ++i){
            et = (CheckBox) recipientsList.getChildAt(i).findViewById(R.id.friendSelectBox);
            if (et!=null)  et.setChecked(false);
        }
        ((CustomFriendSelectedListAdapter)((HeaderViewListAdapter)recipientsList.getAdapter()).getWrappedAdapter()).friendsSelected = new ArrayList<>();
    }


    @Override
    public void changeOrientation(int orientation) {

    }


    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        Bitmap pictureBitmap = (Bitmap)params[1];
        ArrayList textViewsList = (ArrayList)params[2];
        TextView coordsText = (TextView)params[3];
        if(params.length == 4) {
            imageString = generateImageString(pictureBitmap, coordsText, textViewsList);
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.applicationFrame, this);
            ft.commit();
        }
    }
}
