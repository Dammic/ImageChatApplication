package com.example.imagechatapplication.ui.fragment.friendsViewFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.ui.adapters.CustomFriendListAdapter;
import com.example.imagechatapplication.ui.adapters.CustomPendingFriendListAdapter;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.interfaces.UpdateObserver;


import java.util.ArrayList;
import java.util.Arrays;

import static com.example.imagechatapplication.services.publicServices.Utils.getEmotsAndWhitespaceFilter;


public class FriendsViewFragment extends Fragment implements ResponsiveFragment, UpdateObserver {
    private View fragmentView;
    private OrientationHandler orientationHandler;

    CustomFriendListAdapter adapter;
    CustomPendingFriendListAdapter adapter1;

    private ListView friendsList;
    private ListView friendRequestsList;
    private Button addFriendRequestButton;
    private EditText friendRequestText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_viewgroup_friends, container, false);

        //initializing layout components
        friendsList = (ListView)fragmentView.findViewById(R.id.friendsList);
        friendRequestsList = (ListView)fragmentView.findViewById(R.id.friendRequestsList);
        addFriendRequestButton = (Button)fragmentView.findViewById(R.id.addFriendRequestButton);
        friendRequestText = (EditText)fragmentView.findViewById(R.id.friendRequestText);


        //setting input filters
        friendRequestText.setFilters(new InputFilter[]{getEmotsAndWhitespaceFilter(), new InputFilter.LengthFilter(25)});

        //setting listeners
        addFriendRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {sendFriendRequest(v);}
        });

        orientationHandler = new OrientationHandler(getActivity(), this);
        orientationHandler.start();


        LayoutInflater inflater1 = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        friendsList.addHeaderView(inflater1.inflate(R.layout.listheader_friend_list, null));
        friendRequestsList.addHeaderView(inflater1.inflate(R.layout.listheader_pending_list, null));

        register();

        update("friendsList");
        update("friendRequestsList");

        Log.e("friendsView","created!");

        return fragmentView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e("friendsView","resumed!");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        orientationHandler.stop();
        unregister();
    }


    //@todo: move setting text to "" after a successful sending friend request (bug: now when we send a friend request and it fails, it cleans the field)
    public void sendFriendRequest(View v){
        ConnectionManager.getInstance().getConnectionUtils().sendFriendRequest(friendRequestText.getText().toString(), getActivity());
        friendRequestText.setText("");
    }

    @Override
    public void changeOrientation(int orientation) {

    }

    @Override
    public void update(String observedObject) {
        Object[] dataObjectArray;  FriendInfo[] data;
        if(observedObject.compareTo("friendsList") == 0){
            dataObjectArray = ((ArrayList) ConnectionManager.getInstance().getUpdateSubject().getCurrentFriendsList()).toArray();
            data = Arrays.copyOf(dataObjectArray, dataObjectArray.length, FriendInfo[].class);
            adapter = new CustomFriendListAdapter(getActivity(), data);
            friendsList.setAdapter(adapter);
        }else if(observedObject.compareTo("friendRequestsList") == 0){
            dataObjectArray = ((ArrayList) ConnectionManager.getInstance().getUpdateSubject().getCurrentFriendRequestsList()).toArray();
            data = Arrays.copyOf(dataObjectArray, dataObjectArray.length, FriendInfo[].class);
            adapter1 = new CustomPendingFriendListAdapter(getActivity(), data);
            friendRequestsList.setAdapter(adapter1);
        }
    }

    @Override public void register(){
        ConnectionManager.getInstance().getUpdateSubject().addFriendsListObserver(this);
        ConnectionManager.getInstance().getUpdateSubject().addFriendRequestsListObserver(this);
    }
    @Override public void unregister(){
        ConnectionManager.getInstance().getUpdateSubject().removeFriendsListObserver(this);
        ConnectionManager.getInstance().getUpdateSubject().removeFriendRequestsListObserver(this);
    }

    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        if(params.length == 1) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.viewGroupFrame, this);
            ft.commit();
        }
    }


}
