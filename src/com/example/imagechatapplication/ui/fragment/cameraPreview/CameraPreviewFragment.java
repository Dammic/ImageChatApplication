package com.example.imagechatapplication.ui.fragment.cameraPreview;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.services.applicationServices.TextDragListener;
import com.example.imagechatapplication.services.publicServices.OrientationHandler;
import com.example.imagechatapplication.interfaces.ResponsiveFragment;
import com.example.imagechatapplication.ui.fragment.selectFriendsViewFragment.SelectFriendsViewFragment;
import com.example.imagechatapplication.ui.fragment.viewGroup.ViewGroupFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class CameraPreviewFragment extends Fragment implements ResponsiveFragment {
    private View fragmentView;

    private Bitmap pictureBitmap;
    private ArrayList textViewsList;
    private OrientationHandler orientationHandler;

    private FrameLayout previewFrame;
    private ImageView cameraPreview;
    private ImageButton addNewTextButton;
    private TextView coordsText;
    private Button okButton;
    private Button denyButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        fragmentView = inflater.inflate(R.layout.fragment_camera_preview, container, false);

        //initializing layout components
        previewFrame = (FrameLayout)fragmentView.findViewById(R.id.previewFrame);
        cameraPreview = (ImageView)fragmentView.findViewById(R.id.cameraPreview);
        addNewTextButton = (ImageButton)fragmentView.findViewById(R.id.addNewTextButton);
        coordsText = (TextView)fragmentView.findViewById(R.id.coordsText);
        okButton = (Button)fragmentView.findViewById(R.id.okButton);
        denyButton = (Button)fragmentView.findViewById(R.id.denyButton);

        cameraPreview.setScaleType(ImageView.ScaleType.FIT_XY);
        cameraPreview.setImageBitmap(pictureBitmap);

        //setting listeners
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {okClicked(v);}
        });
        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {denyClicked(v);}
        });
        addNewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {addNewText(v);}
        });


        //other settings
        orientationHandler = new OrientationHandler(getActivity(), this);
        orientationHandler.start();

        textViewsList = new ArrayList<EditText>();
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            coordsText.setText("Unknown localization");
        }else{
            Location location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Geocoder gcd = new Geocoder(getActivity().getBaseContext(), Locale.getDefault());
            String cityName = null;
            try{
                List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if(addresses.size() > 0)  cityName = addresses.get(0).getLocality();
                else cityName = "Unknown city";
                coordsText.setText("|"+cityName+"| Lat: "+location.getLatitude()+" Lon: "+location.getLongitude());
            }catch (Exception e) {e.printStackTrace(); Log.e("GPS ERROR", "Please initialize GPS!");  coordsText.setText("Unknown localization");}
        }



        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {



    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        orientationHandler.stop();
    }


    public void addNewText(View v){

        EditText editText = new EditText(getActivity());
        editText.setHint("Text");
        editText.setTextSize(22);
        editText.setMaxLines(5);
        editText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        editText.setEnabled(true);
        editText.setMinWidth(120);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        params.setMargins(0,50,0,0);

        editText.setLayoutParams(params);

        editText.setOnTouchListener(new TextDragListener(editText, textViewsList));

        previewFrame.addView(editText);
        editText.requestFocus();
        ((InputMethodManager)editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        textViewsList.add(editText);
    }


    //@todo: will we need that? (does the fragment get completely erased?
    public void clearAllEditTexts(){
        for(Object editText: textViewsList)  previewFrame.removeView((EditText)editText);
        textViewsList = new ArrayList<EditText>();
    }

    //@todo: too quick clicking on this button may cause crash: make a photo and mash the place where this "x" button will be
    public void denyClicked(View v){
        clearAllEditTexts();
        new ViewGroupFragment().showFragment(getActivity());
        //mCameraView.getCamera().startPreview();

    }

    public void okClicked(View v){
        new SelectFriendsViewFragment().showFragment(getActivity(), pictureBitmap, textViewsList, coordsText);
    }

    @Override
    public void changeOrientation(int orientation) {

    }


    @Override
    public void showFragment(Object... params){
        Activity activity = (Activity)params[0];
        pictureBitmap = (Bitmap)params[1];
        if(params.length == 2) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.applicationFrame, this);
            ft.commit();
        }
    }
}
