package com.example.imagechatapplication.ui.adapters;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.example.imagechatapplication.ui.fragment.cameraView.CameraViewFragment;
import com.example.imagechatapplication.ui.fragment.friendsViewFragment.FriendsViewFragment;
import com.example.imagechatapplication.ui.fragment.imagesViewFragment.ImagesViewFragment;

import java.util.ArrayList;

public class ViewGroupPagerAdapter extends FragmentPagerAdapter {

    static final int NUM_ITEMS = 3;
    private ArrayList<Fragment> fragmentList;
    private FragmentManager fm;
    private Activity activity;


    public ViewGroupPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.fm = fm;
        this.activity = activity;
        fragmentList = new ArrayList();
        fragmentList.add(new ImagesViewFragment());
        fragmentList.add(new CameraViewFragment());
        fragmentList.add(new FriendsViewFragment());
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public void removeAllfragments() {
        if (fragmentList != null && !activity.isFinishing()) {
            for (Fragment fragment : fragmentList) fm.beginTransaction().remove(fragment).commit();
            fragmentList.clear();
            notifyDataSetChanged();
        }
        fragmentList = null;
    }

}
