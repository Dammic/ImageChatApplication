package com.example.imagechatapplication.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.ImageInfo;
import com.example.imagechatapplication.services.publicServices.Loader;
import org.jdeferred.DoneCallback;


public class CustomImagesListAdapter extends ArrayAdapter<ImageInfo>{
	Context context;
	ImageInfo data[] = null;
    Loader loader;
	
	public CustomImagesListAdapter(Context context, ImageInfo[] data) {
        super(context, R.layout.listelement_images, data);
        this.context = context;
        this.data = data;
        this.loader = new Loader((Activity)context);
    }
    
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ImageHolder holder;
       
        if(row == null){
        	
        	LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		row = inflater.inflate(R.layout.listelement_images, parent, false);
           
            holder = new ImageHolder();
            holder.sender = (TextView)row.findViewById(R.id.imageSender);
           
            row.setTag(holder);
        }else  holder = (ImageHolder)row.getTag();

        ImageInfo imageInfo = data[position];
       
        holder.sender.setText("New picture from: "+imageInfo.sender);
        holder.stamp = imageInfo.imageID;
        
        holder.sender.setOnClickListener(
        	new View.OnClickListener(){             
               public void onClick(View v){
                   loader.show();
                   ConnectionManager.getInstance().getConnectionUtils().getImageDetail(holder.stamp, (Activity)v.getContext())
                       .done(new DoneCallback() {
                           @Override public void onDone(Object o)  {loader.hide();}
                       });
               }
             }
        );
        return row;
    }

    class ImageHolder{
        TextView sender;
        String stamp;
	}
	

    
}
