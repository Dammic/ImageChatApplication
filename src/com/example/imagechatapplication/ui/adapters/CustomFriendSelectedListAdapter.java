package com.example.imagechatapplication.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.view.LayoutInflater;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.services.publicServices.Loader;

import java.util.ArrayList;


public class CustomFriendSelectedListAdapter extends ArrayAdapter<FriendInfo>{
	Context context;
	int layoutResourceId;  
	FriendInfo data[] = null;


    public ArrayList<String> friendsSelected;
    Loader loader;
	
	public CustomFriendSelectedListAdapter(Context context, FriendInfo[] data) {
        super(context, R.layout.listelement_friend_list, data);
        this.context = context;
        this.data = data;
        this.loader = new Loader((Activity)context);
        friendsSelected = new ArrayList<>();
    }
    
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final FriendSelectedInfoHolder holder;
       
        if(row == null){
        	
        	LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		row = inflater.inflate(R.layout.listelement_friend_select, parent, false);
           
            holder = new FriendSelectedInfoHolder();
            holder.friendName = (TextView)row.findViewById(R.id.friendSelectName);
            holder.friendSelected = (CheckBox)row.findViewById(R.id.friendSelectBox);
           
            row.setTag(holder);
        }else  holder = (FriendSelectedInfoHolder)row.getTag();

        holder.id = data[position].id;
        holder.friendName.setText(data[position].userName);
        holder.friendSelected.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
	            if(holder.friendSelected.isChecked())
                    friendsSelected.add(holder.id);
	        	 else  friendsSelected.remove(holder.id);
	            
	            Button sendButton = (Button)((Activity) v.getContext()).findViewById(R.id.sendButton);
	            
	            if(friendsSelected.size() > 0)
	            	sendButton.setEnabled(true);
	            else sendButton.setEnabled(false);
            }
        });
                        		
        return row;
    }

    class FriendSelectedInfoHolder{
        TextView friendName;
        CheckBox friendSelected;
        String id;
    }
    
}
