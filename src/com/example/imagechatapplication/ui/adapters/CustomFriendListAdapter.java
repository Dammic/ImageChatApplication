package com.example.imagechatapplication.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.services.publicServices.Loader;
import org.jdeferred.DoneCallback;


public class CustomFriendListAdapter extends ArrayAdapter<FriendInfo>{
	Context context;
	FriendInfo data[] = null;
    Loader loader;
	
	public CustomFriendListAdapter(Context context, FriendInfo[] data) {
        super(context, R.layout.listelement_friend_list, data);
        this.context = context;
        this.data = data;
        this.loader = new Loader((Activity)context);

    }

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final FriendInfoHolder holder;
       
        if(row == null){
        	
        	LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		row = inflater.inflate(R.layout.listelement_friend_list, parent, false);
           
            holder = new FriendInfoHolder();
            holder.friendName = (TextView)row.findViewById(R.id.friendName);
            holder.deleteFriendButton = (Button)row.findViewById(R.id.deleteFriendButton);
           
            row.setTag(holder);
        }else  holder = (FriendInfoHolder)row.getTag();
        holder.id = data[position].id;
        holder.friendName.setText(data[position].userName);
        holder.deleteFriendButton.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    loader.show();
                    ConnectionManager.getInstance().getConnectionUtils().deleteFriend(holder.id, (Activity) v.getContext())
                    .done(new DoneCallback() {
                        @Override
                        public void onDone(Object o) {
                            loader.hide();
                        }
                    });
                }
            }
        );
        return row;
    }

    class FriendInfoHolder{
        TextView friendName;
        Button deleteFriendButton;
        String id;
    }
    
}
