package com.example.imagechatapplication.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.classes.FriendInfo;
import com.example.imagechatapplication.services.publicServices.Loader;
import org.jdeferred.DoneCallback;


public class CustomPendingFriendListAdapter extends ArrayAdapter<FriendInfo>{
	Context context;
	int layoutResourceId;  
	FriendInfo data[] = null;
	Loader loader;
	
	public CustomPendingFriendListAdapter(Context context, FriendInfo[] data) {
        super(context, R.layout.listelement_pending_friend, data);
        this.context = context;
        this.data = data;
		this.loader = new Loader((Activity)context);
    }
    
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final PendingFriendInfoHolder holder;
       
        if(row == null){
        	
        	LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		row = inflater.inflate(R.layout.listelement_pending_friend, parent, false);
           
            holder = new PendingFriendInfoHolder();
            holder.pendingFriendName = (TextView)row.findViewById(R.id.pendingFriendName);
            holder.denyFriendButton = (Button)row.findViewById(R.id.denyFriend);
            holder.acceptFriendButton = (Button)row.findViewById(R.id.acceptFriend);
           
            row.setTag(holder);
        }else  holder = (PendingFriendInfoHolder)row.getTag();
			holder.id = data[position].id;
	        holder.pendingFriendName.setText(data[position].userName);
	        holder.denyFriendButton.setOnClickListener(
	        	new View.OnClickListener(){             
	               public void onClick(View v){
					   loader.show();
					   ConnectionManager.getInstance().getConnectionUtils().deleteFriendRequest(holder.id, (Activity)v.getContext())
						   .done(new DoneCallback() {
							   @Override public void onDone(Object o)  {loader.hide();}
						   });
	               }
	             }
	        );
	        holder.acceptFriendButton.setOnClickListener(
	            	new View.OnClickListener(){
	                   public void onClick(View v){
						   loader.show();
						   ConnectionManager.getInstance().getConnectionUtils().addFriend(holder.id, (Activity)v.getContext())
							   .done(new DoneCallback() {
								   @Override public void onDone(Object o)  {loader.hide();}
							   });
	                   }
	                 }
	            );
        return row;
    }


    class PendingFriendInfoHolder{
        TextView pendingFriendName;
        Button denyFriendButton;
        Button acceptFriendButton;
		String id;
    }
    
}
