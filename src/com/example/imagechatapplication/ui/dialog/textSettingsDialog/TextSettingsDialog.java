package com.example.imagechatapplication.ui.dialog.textSettingsDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import com.example.imagechatapplication.R;
import java.util.ArrayList;

public class TextSettingsDialog extends Dialog {

    private View observedTextEdit;
    private Button textSettings_setButton;
    private Button textSettings_deleteButton;
    private ImageButton textSettings_cancelButton;
    private GridLayout textSettings_colorGrid;
    private TextView textSettings_preview;
    private ArrayList textEditsList;

    public TextSettingsDialog(View observedTextEdit, ArrayList textEditsList) {
        super(observedTextEdit.getContext());
        this.observedTextEdit = observedTextEdit;
        this.textEditsList = textEditsList;
    }


    //@todo: keyboard should not open after that
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //initializing content views
        setContentView(R.layout.dialog_text_settings);

        //initializing layout components
        textSettings_setButton = (Button)findViewById(R.id.textSettings_setButton);
        textSettings_deleteButton = (Button)findViewById(R.id.textSettings_deleteButton);
        textSettings_cancelButton = (ImageButton)findViewById(R.id.textSettings_cancelButton);
        textSettings_preview = (TextView)findViewById(R.id.textSettings_preview);
        textSettings_colorGrid =(GridLayout)findViewById(R.id.textSettings_colorGrid);

        //setting listeners
        textSettings_setButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {setClicked(v);}
        });
        textSettings_cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {onBackPressed();}
        });
        textSettings_deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {
                FrameLayout previewFrame = (FrameLayout)((Activity)observedTextEdit.getContext()).findViewById(R.id.previewFrame);
                previewFrame.removeView(observedTextEdit);

                textEditsList.remove(observedTextEdit);
                onBackPressed();
            }
        });

        //setting up color grid
        populateColorGrid();

        //setting up preview
        textSettings_preview.setTextColor(((TextView)observedTextEdit).getCurrentTextColor());
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.dismiss();
    }



    public void setClicked(View v) {
        ((TextView)observedTextEdit).setTextColor(textSettings_preview.getCurrentTextColor());


        this.dismiss();
    }


    public void setTextColor(View v){
        int color = ((ColorDrawable)v.getBackground()).getColor();
        textSettings_preview.setTextColor(color);
    }


    //@todo: "google: android slide finger on buttons". When we slide the finger on the buttons, any button under the cursor should get activated, without lifting a finger
    private void populateColorGrid() {
        Resources res = observedTextEdit.getContext().getResources();
        int[] pickColors = res.getIntArray(R.array.pickColors);

        int totalButtons = pickColors.length;             //the number of bottons i have to put in GridLayout
        int buttonsForEveryRow = 6;                       // buttons i can put inside every single row
        int buttonsForEveryRowAlreadyAddedInTheRow = 0;   // count the buttons added in a single rows
        int columnIndex = 0;                              //cols index to which i add the button
        int rowIndex = 0;                                 //row index to which i add the button

        for (int i = 0; i < totalButtons; i++) {
            if (buttonsForEveryRowAlreadyAddedInTheRow == buttonsForEveryRow) {
                rowIndex++;                             //here i increase the row index
                buttonsForEveryRowAlreadyAddedInTheRow = 0;
                columnIndex = 0;
            }
            ImageButton imageButton = new ImageButton(observedTextEdit.getContext());
            imageButton.setBackgroundColor(pickColors[i]);
            imageButton.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View v, MotionEvent event) {
                   setTextColor(v);
                   return true;
               }
           });

            GridLayout.Spec row = GridLayout.spec(rowIndex, 1);
            GridLayout.Spec colspan = GridLayout.spec(columnIndex, 1);
            GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(row, colspan);
            textSettings_colorGrid.addView(imageButton, gridLayoutParam);

            buttonsForEveryRowAlreadyAddedInTheRow++;
            columnIndex++;
        }
    }
}
