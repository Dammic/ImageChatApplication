package com.example.imagechatapplication.ui.dialog.textSettingsDialog;

import android.util.Log;
import android.view.View;
import android.view.Window;
import com.example.imagechatapplication.ui.dialog.textSettingsDialog.TextSettingsDialog;

import java.util.ArrayList;


public class TextSettingsDialogHandler implements Runnable {
    private View observedElement;
    private boolean[] isOnClick;
    private ArrayList textEditsList;

    public TextSettingsDialogHandler(View v, boolean[] isOnClick, ArrayList textEditsList){
        super();
        observedElement = v;
        this.isOnClick = isOnClick;
        this.textEditsList = textEditsList;
    }

    @Override
    public void run() {
        onLongClicked();
    }


    private void onLongClicked() {
        isOnClick[0] = false;
        TextSettingsDialog textSettingsDialog = new TextSettingsDialog(observedElement, textEditsList);
        textSettingsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        textSettingsDialog.show();
    }


}
