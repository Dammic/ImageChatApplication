package com.example.imagechatapplication.ui.dialog.registerDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.example.imagechatapplication.R;
import com.example.imagechatapplication.connection.ConnectionManager;
import com.example.imagechatapplication.services.publicServices.Loader;
import org.jdeferred.DoneCallback;


import static com.example.imagechatapplication.services.publicServices.Utils.getEmotsAndWhitespaceFilter;

public class RegisterDialog extends Dialog {
    private TextView register_loginBox;
    private TextView register_passwordBox;
    private TextView register_passwordConfirmBox;
    private Button register_registerButton;
    private ImageButton register_exitButton;
    private Activity activity;
    private Loader loader;

    public RegisterDialog(Context context) {
        super(context);
        activity = (Activity)context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e("creating","dialog!");
        //initializing content views
        setContentView(R.layout.dialog_register);
        loader = new Loader(activity);

        //initializing layout components
        register_loginBox = (TextView)findViewById(R.id.register_loginBox);
        register_passwordBox = (TextView)findViewById(R.id.register_passwordBox);
        register_passwordConfirmBox = (TextView)findViewById(R.id.register_passwordConfirmBox);
        register_registerButton = (Button)findViewById(R.id.register_registerButton);
        register_exitButton = (ImageButton)findViewById(R.id.register_exitButton);

        //setting listeners
        register_registerButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {registerClicked(v);}
        });
        register_exitButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)  {onBackPressed();}
        });

        //setting filters to the inputs
        InputFilter[] emoticonAndWhitespaceFilter = new InputFilter[]{getEmotsAndWhitespaceFilter(), new InputFilter.LengthFilter(25)};
        register_loginBox.setFilters(emoticonAndWhitespaceFilter);
        register_passwordBox.setFilters(emoticonAndWhitespaceFilter);
        register_passwordConfirmBox.setFilters(emoticonAndWhitespaceFilter);
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        register_loginBox.setText("");
        register_passwordBox.setText("");
        register_passwordConfirmBox.setText("");
        this.dismiss();
    }



    public void registerClicked(View v) {
        if( register_loginBox.length() < 5 ||
            register_passwordBox.length() < 5 ||
            register_passwordBox.length() > 20 ||
            register_passwordConfirmBox.getText().toString().compareTo(register_passwordBox.getText().toString()) != 0){
            Toast.makeText(activity, "Invalid login or password! (>=5,<=20)", Toast.LENGTH_LONG).show();
        }else {
            loader.show();
            ConnectionManager.getInstance().getConnectionUtils().register(register_loginBox.getText().toString(), register_passwordBox.getText().toString(), activity)
                .done(new DoneCallback() {
                    @Override public void onDone(Object o) {
                        ConnectionManager.getInstance().getConnectionUtils().loginByToken(activity).done(new DoneCallback() {
                            @Override public void onDone(Object o)  {loader.hide();}
                        });
                    }
                });
        }
    }





}
