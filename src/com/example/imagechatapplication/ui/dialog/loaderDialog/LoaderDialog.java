package com.example.imagechatapplication.ui.dialog.loaderDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.example.imagechatapplication.R;


public class LoaderDialog extends Dialog {
    Activity activity;

    public LoaderDialog(Context context) {
        super(context);
        this.activity = (Activity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        //initializing content views
        setContentView(R.layout.dialog_loader);
    }


    //@todo: cancel on onBackPressed()
    @Override
    public void onBackPressed(){}

}
