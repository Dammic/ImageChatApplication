package com.example.imagechatapplication.interfaces;

public interface UpdateObserver{
    void update(String updatedObject);
    void register();
    void unregister();
}
