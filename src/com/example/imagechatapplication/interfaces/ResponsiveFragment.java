package com.example.imagechatapplication.interfaces;


public interface ResponsiveFragment {
    void showFragment(Object... params);
    void changeOrientation(int orientation);
}
