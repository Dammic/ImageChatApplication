package com.example.imagechatapplication.interfaces;

import android.annotation.TargetApi;
import android.os.Build;
import com.example.imagechatapplication.classes.ConnectionWrapper;

public interface ConnectionStrategy {

    @TargetApi(Build.VERSION_CODES.KITKAT)
    ConnectionWrapper doInBackground(Object ...params);
}
