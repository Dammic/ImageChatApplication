package com.example.imagechatapplication.interfaces;

import android.graphics.Bitmap;


public interface Image {

    public Bitmap setPicture(Bitmap picture);

}
